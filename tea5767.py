#!/usr/bin/env python3
#Reference code obtained from "https://www.raspberrypi.org/forums/viewtopic.php?t=53680" by user: LinuxCircle

import smbus as smbus 
import subprocess
import curses
import time

import quick2wire.i2c as i2clib

i2c = smbus.SMBus(1) # newer version RASP (512 megabytes)
i2c_address = 0x60

class Bus:
    """Provides an abstraction of the actual bus."""
    def __init__(self, i2c, address):
        self.i2c = i2c
        self.bus = i2clib.I2CMaster()
        self.address = address

    def init(self):
        """initialize hardware"""
        self.i2c.write_quick(self.address)
        time.sleep(0.1)

    def write(self, data):
        first = data[0]
        rest = data[1:]
        self.i2c.write_i2c_block_data(self.address, first, rest)

    def read(self):
        results = self.bus.transaction(
            i2clib.reading(self.address, 5)
        )
        data = [ int(d) for d in results[0] ]
        return data


class Radio:
    """A simple class to controll a 5767 radio module over I2C bus"""

    def __init__(self, bus, frequency):
        self.bus = bus
        self.frequency = frequency
        self.muted = True
        self.trace = True

    def set_freq(self, frequency, step=100000):
        """Set Radio to specific frequency. Frequency is an integer with a step of 100kHz"""

        freq14bit = self.from_freq(frequency,step)
        freqH = (freq14bit >> 8) & 0x3f
        freqL = freq14bit & 0xFF

        if self.trace:
            print("Setting the frequency coefficient to: %d" % freq14bit)

        data = [
            freqH, # freqH # 1.byte (MUTE bit; Frequency H)  // MUTE is 0x80
            freqL, # Byte #2 (frequency L)
            0xB0,  # 0b10110000 # byte #3 (SUD; SSL1, SSL2; HLSI, MS, MR, ML; SWP1)
            0x10,  # 0b00010000 # byte #4 (SWP2; STBY, BL; XTAL; SMUTE; HCC, SNC, SI)
            0x00   # 0b00000000 # byte #5 (PLREFF; DTC; 0; 0; 0; 0; 0; 0)
        ]

        self.bus.write(data)
        self.frequency = frequency
        if self.trace:
            print("Frequency set to: " + str(frequency/10.) + "MHz")

    def get_freq(self):
        data = self.bus.read()
        print("Recevied data: length = " + str(len(data)) + " data: " + str(data))
        self.dump_state(data)

    def mute(self):
        """"mute radio"""

        freq14bit = self.from_freq(self.frequency)
        freqH = (freq14bit >> 8) & 0x3f
        freqL = freq14bit & 0xFF

        data = [ 0x80 | freqH, freqL, 0xB0, 0x10, 0x00 ]

        self.bus.write(data)
        self.muted = True
        if self.trace:
            print("Radio Muted")

    def toggle(self):
        if self.muted:
            self.set_freq(self.frequency)
            self.muted = False
        else:
            self.mute()

    def from_freq(self, freq, step = 100000):
        """Compute the device coefficient for the given frequency (in units of 100kHz)"""
        return int((freq * step + 225000) / 8192. + 0.5) & ((1 << 14) - 1)

    def to_freq(self, f_coeff):
        return (f_coeff * 8192 - 225000) / 1000000.

    def dump_state(self, data):
        print("Byte 1[7]: RF = %d" % (1 if data[0] & 0x80 != 0 else 0) )
        print("Byte 1[6]: BLF = %d" % (1 if data[0] & 0x40 != 0 else 0) )
        f_coeff = (data[0] & 0x3f) << 8 | data[1]
        print("f_coeff = %d" % f_coeff)
        print("F: %f MHz" % self.to_freq(f_coeff))
        print("Byte 3: %s" % hex(data[2]))
        print("  STEREO: %d" % (1 if data[2] & 0x80 != 0 else 0))
        print("Byte 4: %s" % hex(data[3]))
        print("  LEV: %d" % (data[3]>>4))
        print("Byte 5: %s" % hex(data[4]))

if __name__ == '__main__':

    bus = Bus(i2c, i2c_address)
    bus.init()

    frequency = 1040 # sample starting frequency
    radio = Radio(bus, frequency)

    # terminal user input infinite loop
    stdscr = curses.initscr()
    curses.noecho()

    radio.set_freq(frequency)

    delay = 0.5

    try:
        while True:
            c = stdscr.getch()
            if c == ord('1'): # set to 101.1
                frequency = 1040
                radio.set_freq(frequency)
                time.sleep(delay)
            elif c == ord('v'): # set to 102.1
                frequency = 1021
                radio.set_freq(frequency)
                time.sleep(delay)
            elif c == ord('w'): # increment by 1
                frequency += 10
                radio.set_freq(frequency)
                time.sleep(delay)
            elif c == ord('s'): # decrement by 1
                frequency -= 10
                radio.set_freq(frequency)
                time.sleep(delay)
            elif c == ord('e'): # increment by 0.1
                frequency += 1
                radio.set_freq(frequency)
                time.sleep(delay)
            elif c == ord('d'): # decrement by 0.1
                frequency -= 1
                radio.set_freq(frequency)
                time.sleep(delay)
            elif c == ord('m'): # mute
                radio.toggle()
                time.sleep(delay)
            elif c == ord('u'): # unmute
                radio.set_freq(frequency)
                time.sleep(delay)
            elif c == ord('p'):
                radio.get_freq()
                time.sleep(delay)
            elif c == ord('q'): # exit script and cleanup
                radio.mute()
                curses.endwin()
                break
            elif c == ord('a'): # exit script and keep radio on
                curses.endwin()
                break
    except KeyboardInterrupt:
        radio.mute()
        curses.endwin()
    except IOError:
        subprocess.call(['i2cdetect', '-y', '1'])
    except Exception as e:
        radio.mute()
        curses.endwin()
        print(e)
